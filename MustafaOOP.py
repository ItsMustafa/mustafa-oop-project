#Parent class
class Pets:
    Cats = []
    def __init__(self, Cats):
        self.Cats = Cats
    def walk(self):
        for Cat in self.Cats:
            print(Cat.walk())
            
#Parent class
class Cat:
    # class attribute
    species = 'mammal'
    
#Inisialisasi Attribut Class
    def __init__(self, name, color, age):
        self.name = name
        self.color = color
        self.age = age
#Instance Method        
    def description(self):
        print(self.name + " is a beautiful " + str(self.age) + " year(s) old " + self.color + " cat.")    
#Instance Method    
    def speak(self, sound):
        return "{} says {}".format(self.name, sound)
#Instance Method    
    def walk(self):
        return "%s is walking" % (self.name)    

#Child class (pewarisan dari class Cat)
class Ragdoll(Cat):
    def fur(self, length):
        return "{}'s fur is {}".format(self.name, length)
    
#Child class (pewarisan dari class Cat)
class Persian(Cat):
    def fur(self, length):
        return "{}'s fur is {}".format(self.name, length)
    
# Create instances of Cats
my_Cats = [
    Ragdoll("Tom", "White", 3), 
    Persian("Kitty", "Black", 4), 
    Cat("Garfield", "Orange", 6)
]
# Instantiate the Pets class
my_pets = Pets(my_Cats)
# Output
print("I have {} Cats.".format(len(my_pets.Cats)))
for Cat in my_pets.Cats:
    print("{} is {} year(s) old and it's a beautiful {} cat.".format(Cat.name, Cat.age, Cat.color))
print("And they're all {}s, of course.".format(Cat.species))
my_pets.walk()



    
    
